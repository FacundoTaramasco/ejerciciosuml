/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml2;

/**
 *
 * @author spirok
 */
public class Main {
    
    public static void main(String[] args) {
        Comprobante cb = new Comprobante('a', 1010, new Fecha(10, 11, 14) );
        System.out.println("Comprobante cb creado : \n" + cb);
        
        
        Recibo rb = new Recibo('b',
                               999,
                               new Fecha(10, 10, 10),
                               new Proveedor(4545, "monotributista"),
                               1234.56,
                               "Detalle1");
        System.out.println("Recibo rb creado : \n" + rb);
            
    }
}
