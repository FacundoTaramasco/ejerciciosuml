/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml2;

/**
 *
 * @author spirok
 */
public class Fecha {

    private int dia;
    private int mes;
    private int anio;
    
    /**
     * Constructor
     * @param dia
     * @param mes
     * @param anio
     */
    public Fecha(int dia, int mes, int anio) {
        setDia(dia);
        setMes(mes);
        setAnio(anio);
    }
    
    // Getters
    public int getDia() {
        return this.dia;
    }
    public int getMes() {
        return this.mes;
    }
    public int getAnio() {
        return this.anio;
    }
 
    // Setters
    public void setDia(int dia) {
        this.dia = dia;
    }
    public void setMes(int mes) {
        this.mes = mes;
    }
    public void setAnio(int anio) {
        this.anio = anio;
    }
    
    // Customs
    @Override
    public String toString() {
        return getDia() + "/" + getMes() + "/" + getAnio();
    }    
    
}
