/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml2;

/**
 *
 * @author spirok
 */
public class Comprobante {
    
    private char tipo;
    private int numero;
    private Fecha fecha;
    
    /**
     * Constructor
     * @param tipo
     * @param numero
     * @param fecha
     */
    public Comprobante(char tipo, int numero, Fecha fecha) {
        setTipo(tipo);
        setNumero(numero);
        setFecha(fecha);
    }

    // Getters
    public char getTipo() {
        return this.tipo;
    }
    public int getNumero() {
        return this.numero;
    }
    public Fecha getFecha() {
        return this.fecha;
    }
    
    
    // Setters
    public void setTipo(char tipo) {
        this.tipo = tipo;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }
    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }
    
    // Customs
    @Override
    public String toString() {
        return "Tipo : " +getTipo() + "\n" +
               "Numero : " + getNumero() + "\n" +
               "Fecha : " + getFecha() + "\n";
    }
    
}
