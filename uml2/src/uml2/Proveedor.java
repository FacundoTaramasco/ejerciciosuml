/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml2;

/**
 *
 * @author spirok
 */
public class Proveedor {
    
    private int codigo;
    private String razonSocial;
    
    /**
     * Constructor
     * @param codigo
     * @param razonSocial
     */
    public Proveedor(int codigo, String razonSocial) {
        setCodigo(codigo);
        setRazonSocial(razonSocial);
    }
    
    // Getters
    public int getCodigo() {
        return this.codigo;
    }
    public String getRazonSocial() {
        return this.razonSocial;
    }
    
    // Setters
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    // Customs
    @Override
    public String toString() {
        return "[Codigo : " + getCodigo() + " RazonSocial : " + getRazonSocial()+"]";
    }
}
