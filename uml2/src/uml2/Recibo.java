/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uml2;

/**
 *
 * @author spirok
 */
public class Recibo extends Comprobante {

    private Proveedor proveedor;
    private double total;
    private String detalle;
        
    /**
     * Constructor
     * @param tipo
     * @param numero
     * @param fecha
     * @param proveedor
     * @param total
     * @param detalle
     */
    public Recibo(char tipo, int numero, Fecha fecha,
                  Proveedor proveedor, double total, String detalle) {
        super(tipo, numero, fecha);
        setProveedor(proveedor);
        setTotal(total);
        setDetalle(detalle);
    }
    
    // Getters
    public Proveedor getProveedor() {
        return this.proveedor;
    }
    public double getTotal() {
        return this.total;
    }
    public String getDetalle() {
        return this.detalle;
    }
    
    // Setters
    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
    public void setTotal(double total) {
        this.total = total;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    
    // Customs
    @Override
    public String toString() {
        return super.toString()  +
               "Proveedor : "    + getProveedor() + "\n" +
               "Total $ : "      + getTotal()     + "\n" +
               "Detalle : "      + getDetalle();
    }
}
