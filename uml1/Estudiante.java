

public class Estudiante extends Persona {

    private String carrera;
    private int legajo;
    
    // Constructores sobrecargados
    public Estudiante() {
        this("");
    }
    public Estudiante(String nombre) {
        this(nombre, 0);
    }
    public Estudiante(String nombre, int edad) {
        this(nombre, edad, "");
    }
    public Estudiante(String nombre, int edad, String carrera) {
        this(nombre, edad, carrera, 0);
    }
    public Estudiante(String nombre, int edad, String carrera, int legajo) {
        super(nombre, edad); // llamo al constructor de Persona
        setCarrera(carrera);
        setLegajo(legajo);
    }

    // Getters
    public String getCarrera() {
        return this.carrera;
    }
    public int getLegajo() {
        return this.legajo;
    }
    
    // Setters
    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }
    
    // Customs
    public String toString() {
        return super.toString() + "\n" +
               "Carrera : " + getCarrera() + "\n"+
               "Legajo  : " + getLegajo();
    }
}
