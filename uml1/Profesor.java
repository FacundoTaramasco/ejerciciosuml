
public class Profesor extends Persona {

    private String materia;
    private String cargo;
    
    // Constructor sobrecargado
    public Profesor() {
        this("");
    }
    public Profesor(String nombre) {
        this(nombre, 0);
    }
    public Profesor(String nombre, int edad) {
        this(nombre, edad, "");
    }
    public Profesor(String nombre, int edad, String materia) {
        this(nombre, edad, materia, "");
    }
    public Profesor(String nombre, int edad, String materia, String cargo) {
        super(nombre, edad); // llamo al constructor de Persona
        setMateria(materia);
        setCargo(cargo);
    }
    
    // Getters
    public String getMateria() {
        return this.materia;
    }
    public String getCargo() {
        return this.cargo;
    }
    
    // Setters
    public void setMateria(String materia) {
        this.materia = materia;
    }
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    // Customs
    public String toString() {
        return super.toString() + "\n" +
               "Materia : " + getMateria() + "\n" +
               "Cargo   : " + getCargo();
    }
}
