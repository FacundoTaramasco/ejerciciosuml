

public class Main {

    public static void main(String[] args) {
        Persona nuevaPersona = new Persona("nuevaPerona", 17);
        System.out.println(nuevaPersona);
        
        System.out.println("\n");
        
        Estudiante nuevoEstudiante = new Estudiante("nuevoEstudiante", 19, "Analista Programador", 1234);
        System.out.println(nuevoEstudiante);
        
        System.out.println("\n");
        
        Profesor nuevoProfesor = new Profesor("nuevoProfesor", 40, "POO", "poronga");
        System.out.println(nuevoProfesor);
    }
}
