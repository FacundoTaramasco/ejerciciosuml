

public class Persona {

    private String nombre;
    private int edad;

    // Constructores sobrecargados
    public Persona() {
        this("");
    }
    
    public Persona(String nombre) {
        this(nombre, 0);
    }
    
    public Persona(String nombre, int edad) {
        setNombre(nombre);
        setEdad(edad);
    }
    
    // Getters
    public String getNombre() {
        return this.nombre;
    }
    public int getEdad() {
        return this.edad;
    }
    
    // Setters
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    // Customs
    public String toString() {
        return "Nombre : " + getNombre() + "\n" +
               "Edad   : " + getEdad();
    }
}
