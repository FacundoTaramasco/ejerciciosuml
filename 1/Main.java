
/**
* Write a description of class Main here.
*
* @author (your name)
* @version (a version number or a date)
*/
public class Main {

    public static void main(String[] args) {
        Persona p = new Persona("nombre1", 27);
        System.out.println("persona p : " + p);
        Cliente c = new Cliente("nombre2", 34, "1234566");
        System.out.println("cliente c : " + c);
        Empleado e = new Empleado("nombre3", 67, 4000);
        System.out.println("Empleado e : " + e + " cobra $ : " + e.calcularSalarioNeto());

        Empresa emp = new Empresa("Ubisoft");
        System.out.println("Empresa emp : " + emp);
    }
}
