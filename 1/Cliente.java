
/**
 * Write a description of class Cliente here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Cliente extends Persona {

    private String telefono;

    public Cliente(String nombre, int edad, String telefono) {
        super(nombre, edad);
        setTelefono(telefono);
    }

    public String getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public String toString() {
        return super.toString() + " " + getTelefono();
    }
}
