
/**
 * Write a description of class Empleado here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Empleado extends Persona {

    private float sueldoBruto;
    private static final double adc = 12.3;

    public Empleado(String nombre, int edad, float sueldoBruto) {
        super(nombre, edad);
        setSueldoBruto(sueldoBruto);
    }
    
    public float getSueldoBruto() {
        return this.sueldoBruto;
    }
    
    public void setSueldoBruto(float sueldoBruto) {
        this.sueldoBruto = sueldoBruto;
    }
    
    public String toString() {
        return super.toString() + " " + getSueldoBruto();
    }
    
    public double calcularSalarioNeto() {
        return getSueldoBruto() * adc;
    }
    
}
