
/**
* Write a description of class Persona here.
*
* @author (your name)
* @version (a version number or a date)
*/
public class Persona {

    private String nombre;
    private int edad;

    public Persona(String nombre, int edad) {
        setNombre(nombre);
        setEdad(edad);
    }

    public String getNombre() {
        return this.nombre;
    }
    public int getEdad() {
        return this.edad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String toString() {
        return getNombre() + " " + getEdad();
    }
}
